# 项目介绍
  bigfans-cloud  https://gitee.com/dafanshudl/bigfans-cloud  的后台管理系统

# 运行步骤
  1. npm install
  2. npm start
  3. 访问 http://localhost:3000

[![star](https://gitee.com/dafanshudl/bigfans-admin/badge/star.svg?theme=dark)](https://gitee.com/dafanshudl/bigfans-admin/stargazers)


# 运行效果

 **1. 商品管理** 

![输入图片说明](https://gitee.com/uploads/images/2018/0418/201833_bce0e433_331009.png "商品管理1.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0418/201842_0aa57e4d_331009.png "商品管理2.png")

 **2. 属性管理** 

![输入图片说明](https://gitee.com/uploads/images/2018/0418/201909_1b5ce4c1_331009.png "属性管理.png")

 **3. 促销管理** 

![输入图片说明](https://gitee.com/uploads/images/2018/0418/201929_c4cd671d_331009.png "促销管理.png")

 **4. 优惠劵管理** 

![输入图片说明](https://gitee.com/uploads/images/2018/0418/201946_f607107a_331009.png "优惠劵管理.png")


最新代码会先push到dev分支 https://gitee.com/dafanshudl/bigfans-admin/tree/dev/

