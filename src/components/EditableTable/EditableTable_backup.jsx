/**
 * Created by lichong on 2017/7/16.
 */

import React from 'react';
import {Table, Input, Popconfirm, Form} from 'antd';
import EditableCell from './EditableCell';
const {Column} = Table;

class EditableTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editable: false,
            data: this.props.data,
            columns: this.props.columns.concat({
                    title: 'operation',
                    dataIndex: 'operation',
                    width: '20%',
                }
            )
        }

        this.getFieldDecorator = this.props.form.getFieldDecorator;
    }

    handleChange(key, index, value) {
        const {data} = this.state;
        data[index][key].value = value;
        this.setState({data});
    }

    edit(index) {
        const {data} = this.state;
        Object.keys(data[index]).forEach((item) => {
            if (data[index][item] && typeof data[index][item].editable !== 'undefined') {
                data[index][item].editable = true;
            }
        });
        this.setState({data});
    }

    editDone(index, type) {
        const {data} = this.state;
        Object.keys(data[index]).forEach((item) => {
            if (data[index][item] && typeof data[index][item].editable !== 'undefined') {
                data[index][item].editable = false;
                data[index][item].status = type;
            }
        });
        this.setState({data}, () => {
            Object.keys(data[index]).forEach((item) => {
                if (data[index][item] && typeof data[index][item].editable !== 'undefined') {
                    delete data[index][item].status;
                }
            });
        });
    }

    renderInputField(columnDef, rowNum) {
        let self = this;
        return (
            <Column
                title={columnDef.title}
                key={columnDef.dataIndex}
                render={
                    (text, record , index) => {
                    const { editable } = this.state.data[index].name;
                    return (
                        <div>
                            <Form.Item>
                                    {
                                        self.getFieldDecorator(`products[${self.props.prodIndex}][${columnDef.dataIndex}]`, 
                                        {
                                        initialValue: record[columnDef.dataIndex]
                                        })(
                                            editable ?
                                            <div>
                                            <Input
                                                onChange={e => self.handleChange(e)}
                                            />
                                            </div>
                                            :
                                            <div className="editable-row-text">
                                            {123|| ' '}
                                            </div>
                                        )
                                }
                                    
                            </Form.Item>
                        </div>
                    )
                }
                }
            />
        )
    }

    renderOperationField(columnDef, rowNum) {
        return (
            <Column
                title='operation'
                key='operation'
                render={(text, record , index) => {
                    const { editable } = this.state.data[index].name;
                    return (
                        <div className="editable-row-operations" key={index}>
                            {
                                editable ?
                                    <span>
                                          <a onClick={() => this.editDone(index, 'save')}>Save</a>
                                          <Popconfirm title="Sure to cancel?" onConfirm={() => this.editDone(index, 'cancel')}>
                                            <a>Cancel</a>
                                          </Popconfirm>
                                        </span>
                                    :
                                    <span>
                  <a onClick={() => this.edit(index)}>Edit</a>
                </span>
                            }
                        </div>
                    )
                }
                }
            />
        );
    }

    renderColumn (columnDef , dataIndex) {

    }

    render() {
        const {data} = this.state;
        const dataSource = data.map((item) => {
            const obj = {};
            Object.keys(item).forEach((key) => {
                obj[key] = key === 'key' ? item[key] : item[key].value;
            });
            return obj;
        });
        const columns = this.state.columns;
        const columnFields = columns.map((columnDef, index) => {
            if (columnDef.dataIndex === 'operation') {
                return this.renderOperationField(columnDef, index);
            }
            if (this.state.editable === false) {
                return (
                    <Column
                        title={columnDef.title}
                        dataIndex={columnDef.dataIndex}
                        key={columnDef.dataIndex}
                    />
                )
            } else {
                if (columnDef.displayType === 'input') {
                    return this.renderInputField(columnDef, index);
                }
                if (columnDef.displayType === 'boolean') {
                    return this.renderBoolean(columnDef, index);
                }
                if (columnDef.displayType === 'select') {
                    return this.renderSelect(columnDef, index);
                }
            }
        })
        return (<Table bordered dataSource={dataSource} pagination={this.props.pagination}>{columnFields}</Table>);
    }
}

export default EditableTable;