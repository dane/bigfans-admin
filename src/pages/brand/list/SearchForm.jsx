import React from 'react';
import {Button ,Form , Row, Col , Input,Cascader , Select} from 'antd'

class SearchForm extends React.Component {

	state = {
		parentCats : []
	}

	handleBrandChange = () => {

	}

	handleReset = () => {
		this.props.form.resetFields();
	}

	handleSearch = (e) => {
		this.props.changeLoading(true);
		this.props.receiveData([]);
	}

	render () {
		const {getFieldDecorator} = this.props.form;
		const formItemLayout = {
	      labelCol: { span: 3 },
	      wrapperCol: { span: 21 },
	    };
		return (
			<Form className="ant-advanced-search-form" onSubmit={this.handleSearch}>
				<Row gutter={24}>
					<Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="分类" >
                            {getFieldDecorator('parentId')(
                                <Cascader placeholder='按分类查找' options={this.state.parentCats} changeOnSelect/>
                            )}
                        </Form.Item>
	                </Col>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="名称" >
		                    {getFieldDecorator('name4')(
		                        <Input/>
		                    )}
		                </Form.Item>
	                </Col>
	                <Col span={8} style={{ display: 'block'}}>
						<Form.Item {...formItemLayout} label="品牌" >
                            {getFieldDecorator('level')(
                                <Select
								    showSearch
								    placeholder="选择品牌"
								    optionFilterProp="children"
								    allowClear
								    onChange={this.handleBrandChange}
								    filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
								  >
								    <Select.Option value="jack">Jack</Select.Option>
								    <Select.Option value="lucy">Lucy</Select.Option>
								    <Select.Option value="tom">Tom</Select.Option>
								  </Select>
                            )}
                        </Form.Item>
	                </Col>
                </Row>
                <Row>
                	<Col span={24} style={{ textAlign: 'right' }}>
			            <Button type="primary" htmlType="submit">Search</Button>
			            <Button style={{ marginLeft: 8 }} onClick={this.handleReset}>
			              Clear
			            </Button>
			        </Col>
                </Row>
			</Form>

			)
	}
}

const SpecSearchForm = Form.create()(SearchForm);
export default SpecSearchForm;
