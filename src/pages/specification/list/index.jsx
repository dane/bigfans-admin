import React from 'react';
import {Table , Breadcrumb ,Card} from 'antd'
import SearchForm from './SearchForm'
import SearchList from './SearchList'

class SpecListPage extends React.Component {

	components = {}

	state = {
	    data: [],
	    pagination: {},
	    loading: false,
	    selectedRowKeys : []
	};

	constructor(props) {
		super(props);
	}

	addComponents(name , component){
		this.components[name] = component
	}

	changeLoading(loading) {
		this.setState({loading})
	}

	receiveData (data) {
		this.setState({data});
		this.setState({loading : false})
	}

	deleteRecord(record){
		let searchForm = this.components['searchForm']
		searchForm.reload()
	}

	render () {
		return (
			<div id="App-product-list">
				<Breadcrumb className="App-breadcrumb">
                    <Breadcrumb.Item>首页</Breadcrumb.Item>
                    <Breadcrumb.Item>规格管理</Breadcrumb.Item>
                    <Breadcrumb.Item>规格列表</Breadcrumb.Item>
                </Breadcrumb>
				<Card bordered={false}>
					<SearchForm 
						changeLoading={(e) => this.changeLoading(e)} 
						receiveData={(e) => this.receiveData(e)} 
						setSearchForm={(e)=>this.addComponents('searchForm' , e)}
					/>
					<SearchList 
						dataSource={this.state.data} 
						loading={this.state.loading} 
						setSearchList={(e)=>this.addComponents('searchList' ,e)}
						onDeleteSpec={(e) => this.deleteRecord(e)}
					/>
				</Card>
			</div>
			)
	}
}

export default SpecListPage;